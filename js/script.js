"use strict";

const btn = document.getElementById('ipcalc');
btn.addEventListener("click", getInfo);

function getInfo() {
    const URI = "https://api.ipify.org/?format=json";

    async function getIP() {
        const response = await fetch(URI);
        const data = await response.json();
        return data;
    }

    getIP()
    .then(({ip}) => {
        async function getAdress() {
            const URIAdrr = "http://ip-api.com/";
            const field = "?fields=status,message,continent,country,region,city,district";
            const responseAdress = await fetch(URIAdrr + "json/" + ip + field);
            const dataAdress = await responseAdress.json();
            return dataAdress;
        }

        getAdress()
        .then(({continent, country, region, city, district}) => {
            const ul = document.createElement('ul');
            btn.after(ul);
            let items = {continent, country, region, city, district};

            for (let key in items) {
                const li = document.createElement('li');
                li.textContent = key + " : " + items[key];
                ul.append(li);
            }

            btn.disabled = true;
        });
    }) 
    .catch((error) => {
        console.log(error.message);
    });
}

